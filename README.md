# react-and-material-with-redux-saga

> In development stage - Paused for undefined time

## Basically

React and Redux SPA in the client-side, Node.js RESTful API in the server-side.

## Motivation

Extract the best from Material UI in a complex React application scope.

Simple Node.js RESTful API to provide mocked data to the front-end.

Create a good UX exploring Material UI components, React Hooks and the React in general; 

In the end we can get an evaluation about the final product versus the code quality and productivity against other UI frameworks, the code conventions, React techniques needed to code with Material UI and the elasticity in unplanned changes / maintenance.

## Points of Interest 

- Newest React features and techniques;
- Whole interface uses Material UI;
- Isomorphic modules;
- React development based in React Hooks techniques to deal with Material UI;
- Redux-Saga middleware for application side-effects and store interactions;
- Node.js server providing backend features to the React client application.

## Reference

 Official websites and documentation from technologies and features used in this project.
 
| # | Link |
|---|------|
|Material-UI|https://material-ui.com/|
|React|https://reactjs.org/|
|Redux|https://redux.js.org/|
|React-Redux|https://react-redux.js.org/|
|Redux-Saga|https://redux-saga.js.org/|
|Node.js|https://nodejs.org/en/|