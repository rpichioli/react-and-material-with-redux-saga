import {takeEvery, put} from 'redux-saga/effects';

import {
	FETCH_RANDOM_JOKE_REQUEST,
	FETCH_RANDOM_JOKE_SUCCESS
} from '../utils/types';

import {ENDPOINT_JOKES_RANDOM} from '../utils/constants';

function* fetchRandomJoke (action) {
	const {term, categories} = action;
	const response = yield fetch(ENDPOINT_JOKES_RANDOM).then(res => res.json());
	yield put({ type: FETCH_RANDOM_JOKE_SUCCESS, joke: response });
}

export default function* rootSaga () {
	yield takeEvery(FETCH_RANDOM_JOKE_REQUEST, fetchRandomJoke);
}
