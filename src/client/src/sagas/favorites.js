import {takeEvery, put} from 'redux-saga/effects';

import {
	ADD_FAVORITE_REQUEST,
	ADD_FAVORITE_SUCCESS,
	REMOVE_FAVORITE_REQUEST,
	REMOVE_FAVORITE_SUCCESS,
	FETCH_RANDOM_JOKE_REQUEST
} from '../utils/types';

/** Return data from state */
export const fetchState = (state) => state.favorites;

/** Add new favorite */
function* addFavorite(action) {
	const {joke} = action;
	yield put({ type: FETCH_RANDOM_JOKE_REQUEST }); // Fetch for new joke
	yield put({ type: ADD_FAVORITE_SUCCESS, joke }); // Add received joke to favorites
}

/** Remove favorite by ID */
function* removeFavorite(action) {
	const {id} = action;
	yield put({ type: REMOVE_FAVORITE_SUCCESS, id });
}

export default function* rootSaga() {
	yield takeEvery(ADD_FAVORITE_REQUEST, addFavorite);
	yield takeEvery(REMOVE_FAVORITE_REQUEST, removeFavorite);
}
