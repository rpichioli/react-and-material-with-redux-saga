import React from 'react';
// Routing
import { Route, Switch } from 'react-router-dom';
// Components
import NotFound from '../components/NotFound';
import Home from '../components/Home';
import About from '../components/About';

/** Componente stateless que centraliza as rotas da aplicação */
export default props => {
	return (
		<Switch>
			<Route exact path="/" component={Home} />
			<Route exact path="/about" component={About} />
			<Route component={NotFound} />
		</Switch>
	)
}
