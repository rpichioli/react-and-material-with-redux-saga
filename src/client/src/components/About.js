import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  title: {paddingBottom: '20px', marginBottom: '20px', borderBottom: '1px dashed #DDD', fontSize: '20px'}
}));

export default function About () {

  const classes = useStyles();

	return (
		<React.Fragment>
			<br />
			<br />
			<Typography variant="h1" gutterBottom className={classes.title}>Details about the project</Typography>
			<p>Coming soon</p>
		</React.Fragment>
	);
}
