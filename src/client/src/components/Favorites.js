import React from 'react';
import {connect} from 'react-redux';
import {removeFavorite} from '../actions/favorites';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { red, pink, purple, deepPurple, indigo, blue, lightBlue, cyan, teal, green, lightGreen, lime, yellow, amber, orange, deepOrange } from '@material-ui/core/colors';

const Favorites = (props) => {

	const classes = useStyles();

	return (
		<React.Fragment>
			<Typography variant="h1" gutterBottom className={classes.title}>My favorites</Typography>

      <Grid container className={classes.grid} spacing={2}>

        {props.favorites.map((item, key) => {
          return (
            <Grid item xs="3" key={key}>
              <Paper elevation={3} className={classes.item}>
                <div className={classes.section1}>
                  <Typography variant="p" color="textSecondary">{item.value}</Typography>
                </div>
                {/* <Divider variant="middle" />
									<div className={classes.section2}>
                  {(item.categories.length === 0) ? (
                    <Chip size="small" className={classes.chipNoCategory} label="No related category" />
                  ) : (
                    item.categories.map(item => <Chip size="small" className={classes.chip} label={item} />)
                  )}
                </div> */}
                <div className={classes.section3}>
                  <Button variant="contained" size="small" className={classes.removeButton} onClick={() => props.removeFavorite(item.id)}>Remove from stash</Button>
                </div>
              </Paper>
            </Grid>
          );
        })}
      </Grid>
		</React.Fragment>
	);
}

const useStyles = makeStyles(theme => ({
	darkText: { color: '#000' },
	detailsText: { fontSize: '14px', fontWeight: 'normal' },
  item: { padding: theme.spacing(1) },
  chip: { marginRight: theme.spacing(1), backgroundColor: cyan[800], },
  chipNoCategory: { marginRight: theme.spacing(1), backgroundColor: '#BBB', },
  section1: { margin: theme.spacing(3, 2), },
  section2: { margin: theme.spacing(2), },
  section3: { margin: theme.spacing(3, 1, 1), justifyContent: 'right' },
  grid: { flexGrow: 1, marginTop: theme.spacing(2)},
  paper: { height: 140, width: 100, },
  large: { width: theme.spacing(7), height: theme.spacing(7), },
  removeButton: {
    backgroundColor: red[600],
    '&:hover': { backgroundColor: red[800], },
    width: '100%'
  },
  favButton: {
    backgroundColor: green[600],
    '&:hover': { backgroundColor: green[800], }
  },
  repickButton: {
    backgroundColor: blue[600],
    '&:hover': { backgroundColor: blue[800], }
  },
  buttonsRoot: {
    '& > *': { margin: theme.spacing(1), },
  },
  extendedIcon: { marginRight: theme.spacing(1), },
  title: {paddingBottom: '20px', marginBottom: '20px', borderBottom: '1px dashed #DDD', fontSize: '20px'}
}));

const mapStateToProps = (state, props) => {
	return {
		favorites: state.favorites || []
	}
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  removeFavorite: (id) => dispatch(removeFavorite(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
