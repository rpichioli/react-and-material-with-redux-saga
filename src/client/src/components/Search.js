import React from 'react';
import {connect} from 'react-redux';
import {onComponentDidMount} from 'react-redux-lifecycle';
import {addFavorite} from '../actions/favorites';
import {fetchRandomJoke} from '../actions/jokes';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';

import Fab from '@material-ui/core/Fab';
import SyncIcon from '@material-ui/icons/Sync';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';

import { red, pink, purple, deepPurple, indigo, blue, lightBlue, cyan, teal, green, lightGreen, lime, yellow, amber, orange, deepOrange } from '@material-ui/core/colors';

const Search = (props) => {

	const classes = useStyles();
  const lastFetchedJoke = props.jokes[0];

	return (
    <React.Fragment>
      <Typography variant="h1" gutterBottom className={classes.title}>Pick your favorite Chuck Norris jokes</Typography>

      {lastFetchedJoke && Object.keys(lastFetchedJoke).length > 0 && (
        <React.Fragment>
          <Typography variant="h5" align="center" className={classes.darkText}>
            "{lastFetchedJoke.value}"
          </Typography>

          <br />

          <Typography variant="h6" align="center" color="textSecondary" className={classes.detailsText}>
            {new Date(lastFetchedJoke.updated_at).toLocaleDateString('pt-br')} {new Date(lastFetchedJoke.updated_at).toLocaleTimeString('pt-br')}
            &nbsp;-&nbsp;
            {(lastFetchedJoke.categories.length === 0) ? "No related category" : lastFetchedJoke.categories.join(', ')}
          </Typography>

          <div style={{textAlign: 'center'}}>
            <div className={classes.buttonsRoot}>
              <Button size="small" className={classes.favButton} variant="contained" onClick={() => props.addFavorite(lastFetchedJoke)}><FavoriteIcon className={classes.extendedIcon} />Add to stash</Button>
              <Button size="small" className={classes.repickButton} variant="contained" onClick={() => props.fetchRandomJoke()}><SyncIcon className={classes.extendedIcon}/>Pick other</Button>
            </div>
          </div>
        </React.Fragment>
      )}
    </React.Fragment>
	);
}

const useStyles = makeStyles(theme => ({
	darkText: { color: '#000' },
	detailsText: { fontSize: '14px', fontWeight: 'normal' },
  item: { padding: theme.spacing(1) },
  chip: { marginRight: theme.spacing(1), backgroundColor: cyan[800], },
  chipNoCategory: { marginRight: theme.spacing(1), backgroundColor: '#BBB', },
  section1: { margin: theme.spacing(3, 2), },
  section2: { margin: theme.spacing(2), },
  section3: { margin: theme.spacing(3, 1, 1), justifyContent: 'right' },
  grid: { flexGrow: 1, marginTop: theme.spacing(2)},
  paper: { height: 140, width: 100, },
  large: { width: theme.spacing(7), height: theme.spacing(7), },
  removeButton: {
    backgroundColor: red[600],
    '&:hover': { backgroundColor: red[800], },
    width: '100%'
  },
  favButton: {
    backgroundColor: green[600],
    '&:hover': { backgroundColor: green[800], }
  },
  repickButton: {
    backgroundColor: blue[600],
    '&:hover': { backgroundColor: blue[800], }
  },
  buttonsRoot: {
    '& > *': { margin: theme.spacing(1), },
  },
  extendedIcon: { marginRight: theme.spacing(1), },
  title: {paddingBottom: '20px', marginBottom: '20px', borderBottom: '1px dashed #DDD', fontSize: '20px'}
}));

const mapStateToProps = (state, props) => {
  return {
    favorites: state.favorites || [],
    jokes: state.jokes || []
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  addFavorite: (joke) => dispatch(addFavorite(joke)),
  fetchRandomJoke: (term, categories) => dispatch(fetchRandomJoke(term, categories))
});

export default connect(mapStateToProps, mapDispatchToProps)(onComponentDidMount(fetchRandomJoke)(Search));
