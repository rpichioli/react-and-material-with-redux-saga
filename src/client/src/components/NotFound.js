import React from 'react'

class NotFound extends React.Component {
	render () {
		return (
			<React.Fragment>
				<h1>Página não encontrada</h1>
				<p>Navegue pelas opções válidas no menu superior.</p>
			</React.Fragment>
		)
	}
}

export default NotFound;
