import React from 'react';
import Typography from '@material-ui/core/Typography';
import Search from './Search';
import Favorites from './Favorites';

class Home extends React.Component {
	render () {
		return (
			<React.Fragment>
				<br />
				<br />
				<Search />
				<br />
				<Favorites />
			</React.Fragment>
		)
	}
}

export default Home;
