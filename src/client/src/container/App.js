import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useTheme, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { red, pink, purple, deepPurple, indigo, blue, lightBlue, cyan, teal, green, lightGreen, lime, yellow, amber, orange, deepOrange } from '@material-ui/core/colors';

import Menu from '../components/Menu'; // App menu
import './App.css';

const theme = createMuiTheme({
	palette: {
    type: 'light',
    primary: indigo,
    secondary: green,
		red: red,
		contrastThreshold: 1,
		tonalOffset: 0.2
  }
});

/** App - Container */
class App extends React.Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <CssBaseline />
				<Menu />
				<Container maxWidth="lg"> {this.props.children}</Container>
			</ThemeProvider>
    );
  }
}

export default App;
