import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
// Routing
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Routes from './routes/Routes'; // All routes
// Redux
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga'
import { loadState, saveState } from './utils/localStorage'; // Synchronization actions to persist data between Redux store and localStorage
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducers/rootReducer'; // Root reducer, used in store
// Redux Saga middlewares
import favoritesSaga from './sagas/favorites';
import jokesSaga from './sagas/jokes';
// Utils
import throttle from 'lodash/throttle'; // Allows only 1x call until defined time expiration
// Container
import App from './container/App'; // Root component

// ------------------------------------------------------------------------------------
// Initial setup
// ------------------------------------------------------------------------------------
// Browser history
const history = createBrowserHistory();
// Saga middleware
const sagaMiddleware = createSagaMiddleware();
// Load data from localStorage synchronized to inject in Redux store again
const persistedState = loadState();
// Creates Redux store assigning Redux Saga like middleware and defining LocalStorage state persisting
const store = createStore(
	rootReducer,
	persistedState,
	composeWithDevTools(applyMiddleware(sagaMiddleware))
);
/** Intercepts each state change adding to localStorage */
store.subscribe(throttle(() => {
	saveState({ favorites: store.getState().favorites }); // Sync only 'favorites' state
}), 2000);
// Registering redux-saga middlewares
sagaMiddleware.run(favoritesSaga);
sagaMiddleware.run(jokesSaga);

// ------------------------------------------------------------------------------------
// Rendering
// ------------------------------------------------------------------------------------
ReactDOM.render(
	<Provider store={store}> {/* Redux store */}
		<Router history={history}> {/* Router containing history */}
			<App> {/* Container */}
				<Routes /> {/* Dynamic component based in routing */}
			</App>
		</Router>
	</Provider>
	, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
