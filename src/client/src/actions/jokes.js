import {FETCH_RANDOM_JOKE_REQUEST} from '../utils/types';

export function fetchRandomJoke (term = '', categories = []) { return ({ type: FETCH_RANDOM_JOKE_REQUEST, term, categories })}
