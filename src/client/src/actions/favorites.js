import { ADD_FAVORITE_REQUEST, REMOVE_FAVORITE_REQUEST } from '../utils/types';

export function addFavorite (joke) { return ({ type: ADD_FAVORITE_REQUEST, joke })}
export function removeFavorite (id) { return ({ type: REMOVE_FAVORITE_REQUEST, id })}
