import {
	FETCH_RANDOM_JOKE_SUCCESS
} from '../utils/types';

export default (state = [], action = {}) => {
	switch (action.type) {
		case FETCH_RANDOM_JOKE_SUCCESS: 
			return [action.joke, ...state];
		default:
			return state;
	}
}
