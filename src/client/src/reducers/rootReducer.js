import { combineReducers } from 'redux';
import favorites from './favorites';
import jokes from './jokes';

// Devolvendo rootReducer contemplando todos os reducers da aplicação para o Redux store
export default combineReducers({ favorites, jokes });
