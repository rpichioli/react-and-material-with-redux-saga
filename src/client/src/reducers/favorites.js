import {
	ADD_FAVORITE_SUCCESS,
	REMOVE_FAVORITE_SUCCESS
} from '../utils/types';

const initialState = [
	{
		"categories": [],
    "created_at": "2020-01-05 13:42:28.664997",
    "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    "id": "lj5tmV9WQpOy0SvKdAZ-rQ",
    "updated_at": "2020-01-05 13:42:28.664997",
    "url": "https://api.chucknorris.io/jokes/lj5tmV9WQpOy0SvKdAZ-rQ",
    "value": "Life is like a box of chocolates until Chuck Norris roundhouse kicks you."
	},
	{
    "categories": [],
    "created_at": "2020-01-05 13:42:25.352697",
    "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    "id": "x2j-n3dkSSat_j_T2OmtNg",
    "updated_at": "2020-01-05 13:42:25.352697",
    "url": "https://api.chucknorris.io/jokes/x2j-n3dkSSat_j_T2OmtNg",
    "value": "Chuck Norris can end the recession with one phone call."
	},
	{
    "categories": [],
    "created_at": "2020-01-05 13:42:26.447675",
    "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    "id": "EBWEZx7iQAaVi_ydISbT9w",
    "updated_at": "2020-01-05 13:42:26.447675",
    "url": "https://api.chucknorris.io/jokes/EBWEZx7iQAaVi_ydISbT9w",
    "value": "Chuck Norris can speak Chinese in German in French in Hindu in English in Algebra in blue in cup with a hint of Russian in Scottish in Canadian"
	},
	{
    "categories": [],
    "created_at": "2020-01-05 13:42:19.324003",
    "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    "id": "6Q-GPUlpS1Svu7RuPyGvcQ",
    "updated_at": "2020-01-05 13:42:19.324003",
    "url": "https://api.chucknorris.io/jokes/6Q-GPUlpS1Svu7RuPyGvcQ",
    "value": "3 asians once ate 150 hotdogs in 1 minute. Not to be out done, Chuck Norris ate 3 asians in less than 20 seconds"
	}
];

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case ADD_FAVORITE_SUCCESS:
			return [action.joke, ...state];
		case REMOVE_FAVORITE_SUCCESS:
			return state.filter(joke => joke.id !== action.id);
		default:
			return state;
	}
}
