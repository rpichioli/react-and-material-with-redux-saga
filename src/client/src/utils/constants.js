export const ENDPOINT_CATEGORIES = 'https://api.chucknorris.io/jokes/categories';
export const ENDPOINT_JOKES_RANDOM_BY_CATEGORY = 'https://api.chucknorris.io/jokes/random?category={TERM}';
export const ENDPOINT_JOKES_RANDOM = 'https://api.chucknorris.io/jokes/random';
export const ENDPOINT_JOKES_SEARCH = 'https://api.chucknorris.io/jokes/search?query={TERM}';
