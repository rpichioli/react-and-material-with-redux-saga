// Load and configure environment variables from .env file
require('dotenv').config();

// Server
const server = require('./controller.js');

// Turn o server by host and port
server.listen(
	process.env.PORT,
	process.env.HOST,
	() => console.log(`Server working at http://${process.env.HOST}:${process.env.PORT}`)
);
