const http = require('http'); // Allow the creation of web servers in both server and client
const url = require('url'); // Helps in prsing URLs

module.exports = http.createServer(async (req, res) => {
	const service = require('./service.js');
  const reqURL = url.parse(req.url, true);
	try {
		// GET Endpoint
	  if (reqURL.pathname == '/sample' && req.method === 'GET') {
	    console.log(`Request Type: ${req.method} Endpoint: ${reqURL.pathname}`);
	    await service.sampleRequest(req, res);
	  // POST Endpoint
		} else if (reqURL.pathname == '/test' && req.method === 'POST') {
	    console.log(`Request Type: ${req.method} Endpoint: ${reqURL.pathname}`);
	    await service.testRequest(req, res);
		// Invalid endpoint
	  } else {
	    console.log(`Request Type: ${req.method} Invalid Endpoint: ${reqURL.pathname}`);
	    await service.invalidRequest(req, res);
	  }
	} catch (e) {
		console.log(`Request Type: ${req.method} Exception: ${e.message}`);
		await service.invalidRequest(req, res);
	}
});
